package com.project.demo;

import com.project.demo.dto.NumberPlateResponseDto;
import com.project.demo.entity.NumberPlate;
import com.project.demo.mapper.NumberPlateMapper;
import com.project.demo.repository.NumberPlateRepository;
import com.project.demo.service.NumberPlateService;
import com.project.demo.service.impl.NumberPlateServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NumberPlatesGenerateTests {
    private final NumberPlateService service;
    private final NumberPlateRepository repository;
    private final NumberPlateMapper mapper;

    public NumberPlatesGenerateTests() {
        this.repository = mock(NumberPlateRepository.class);
        this.mapper = mock(NumberPlateMapper.class);
        this.service = new NumberPlateServiceImpl(repository, mapper);
    }

    @Test
    void testShouldCheckRepositoryAndMapper() {
        String tmpNumPlate = "R 999 RR 111RUS";
        NumberPlate plate = new NumberPlate();
        plate.setCarNumber(tmpNumPlate);
        NumberPlateResponseDto dto = new NumberPlateResponseDto();
        dto.setCarNumber(tmpNumPlate);
        when(repository.findByCarNumber(tmpNumPlate)).thenReturn(Optional.of(plate));
        when(mapper.toDto(plate)).thenReturn(dto);

        System.out.println(dto.getCarNumber());

        Assertions.assertEquals(dto.getCarNumber(), plate.getCarNumber());
    }
}
