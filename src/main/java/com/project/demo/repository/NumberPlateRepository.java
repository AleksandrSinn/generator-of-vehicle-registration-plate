package com.project.demo.repository;

import com.project.demo.entity.NumberPlate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface NumberPlateRepository extends JpaRepository<NumberPlate, Long> {
//    @Query(value = "select p.id from NumberPlate p")
//    List<Long> getAllIds();

    @Query(value = "select count(id) from public.number_plates", nativeQuery = true)
    Integer getCountOfEntities();

    Optional<NumberPlate> findByCarNumber(String carNumber);

    Optional<NumberPlate> findAllByPosition(int position);
}
