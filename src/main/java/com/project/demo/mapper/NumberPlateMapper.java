package com.project.demo.mapper;

import com.project.demo.dto.NumberPlateRequestDto;
import com.project.demo.dto.NumberPlateResponseDto;
import com.project.demo.entity.NumberPlate;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NumberPlateMapper {
    NumberPlateResponseDto toDto(NumberPlate entity);
    NumberPlate toEntity(NumberPlateRequestDto dto);
}
