package com.project.demo.component;

import com.project.demo.entity.NumberPlate;
import com.project.demo.repository.NumberPlateRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Slf4j
@RequiredArgsConstructor
public class SqlInitializationComponent implements ApplicationRunner {
    private final NumberPlateRepository repository;
    private static final AtomicInteger position = new AtomicInteger();

    @Override
    public void run(ApplicationArguments args) {
        repository.deleteAll();
        this.initializeDbOfAllNumberPlates();
        log.info("start AppRunner");
    }

    public void initializeDbOfAllNumberPlates(){
        final char[] alphabet = {'А', 'Е', 'Т', 'О', 'Р', 'Н', 'У', 'К', 'Х', 'С', 'В', 'М'};
        Arrays.sort(alphabet);
        String tmpNumberPlate;
        final String region = "116 RUS";
        char firstLetter;
        char secondLetter;
        char thirdLetter;
        log.info("IN initializeDbOfAllNumberPlates() - start initialize");
        for(char first: alphabet) {
            firstLetter = first;
            for (char second: alphabet){
                secondLetter = second;
                for (char third: alphabet){
                    thirdLetter = third;
                    for(int digitsCounter = 0; digitsCounter < 1000; digitsCounter++){
                        if (digitsCounter < 10) {
                            tmpNumberPlate = String.format("%c00%d%c%c %s", firstLetter, digitsCounter, secondLetter, thirdLetter, region);
                            saveEntity(tmpNumberPlate, position.incrementAndGet());
                        } else if (digitsCounter < 100) {
                            tmpNumberPlate = String.format("%c0%d%c%c %s", firstLetter, digitsCounter, secondLetter, thirdLetter, region);
                            saveEntity(tmpNumberPlate, position.incrementAndGet());
                        }else{
                            tmpNumberPlate = String.format("%c%d%c%c %s", firstLetter, digitsCounter, secondLetter, thirdLetter, region);
                            saveEntity(tmpNumberPlate, position.incrementAndGet());
                        }
                    }
                }
            }
        }
        position.set(0);
        log.info("IN initializeDbOfAllNumberPlates() - finish initialize");
    }

    public void saveEntity(String numberPlate, int position){
        NumberPlate entity = new NumberPlate();
        entity.setCarNumber(numberPlate);
        entity.setPosition(position);
        repository.save(entity);
    }

}
