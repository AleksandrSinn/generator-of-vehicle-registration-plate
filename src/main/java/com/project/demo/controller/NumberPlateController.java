package com.project.demo.controller;

import com.project.demo.dto.NumberPlateRequestDto;
import com.project.demo.dto.NumberPlateResponseDto;
import com.project.demo.service.NumberPlateService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/number")
@RequiredArgsConstructor
public class NumberPlateController {
    private final NumberPlateService numberPlateService;

    @GetMapping("/random")
    public NumberPlateResponseDto getRandomNumber(){
        return numberPlateService.getRandomNumber();
    }

    @GetMapping("/next")
    public NumberPlateResponseDto getNextNumber(){
        return numberPlateService.getNextNumber();
    }

    @PostMapping("/next")
    public NumberPlateResponseDto getNextNumberAfterFixed(@RequestBody NumberPlateRequestDto requestDto){
        return numberPlateService.getNextNumberAfterFixed(requestDto);
    }
}
