package com.project.demo.service.impl;

import com.project.demo.dto.NumberPlateRequestDto;
import com.project.demo.dto.NumberPlateResponseDto;
import com.project.demo.entity.NumberPlate;
import com.project.demo.mapper.NumberPlateMapper;
import com.project.demo.repository.NumberPlateRepository;
import com.project.demo.service.NumberPlateService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Getter
@Service
@RequiredArgsConstructor
public class NumberPlateServiceImpl implements NumberPlateService {
    private final NumberPlateRepository repository;
    private final NumberPlateMapper mapper;

    private List<Integer> randomPositionList;
    private Integer countOfEntities;
    private static final AtomicInteger currentNumber = new AtomicInteger(1);

    @PostConstruct
    public void InitializeNumberPlates(){
        log.info("IN NumberPlateServiceImpl - start to work InitializeNumberPlates()");
        countOfEntities = repository.getCountOfEntities();
        randomPositionList = new ArrayList<>(countOfEntities);
        for(int i = 1; i < countOfEntities; i++){
            randomPositionList.add(i);
        }
        Collections.shuffle(randomPositionList);
        if(countOfEntities == 0){
            log.warn("IN NumberPlateServiceImpl - find {} entities!", (Object) null);
        }
        log.info("IN NumberPlateServiceImpl - finished InitializeNumberPlates()");
    }

    @Override
    public NumberPlateResponseDto getRandomNumber() {
        NumberPlate currentPlate = repository.findAllByPosition(randomPositionList
                        .remove(randomPositionList.size() - 1))
                .orElse(null);
        if(currentPlate == null){
            log.warn("IN getRandomNumber() - not found entity with Position {}", (randomPositionList.size() - 1));
        }
        assert currentPlate != null;
        log.info("IN getRandomNumber() - send random number with position {}", currentPlate.getPosition());
        return mapper.toDto(currentPlate);
    }

    @Override
    public NumberPlateResponseDto getNextNumber() {
        NumberPlate currentNumberPlate;
        if(currentNumber.get() < countOfEntities){
            currentNumberPlate = repository.findAllByPosition(currentNumber.getAndIncrement()).orElse(null);
            if(currentNumberPlate == null){
                log.warn("IN getNextNumber() - return {}", (Object) null);
            }
            assert currentNumberPlate != null;
            log.info("IN getNextNumber() - send next number with position {}", currentNumberPlate.getPosition());
            return mapper.toDto(currentNumberPlate);
        }else {
            log.warn("IN getNextNumber() - all entities are showed. The method has been restarted.");
            currentNumber.set(1);
            return getNextNumber();
        }
    }

    @Override
    public NumberPlateResponseDto getNextNumberAfterFixed(NumberPlateRequestDto requestDto) {
        String tmpNumber = requestDto.getCarNumber();
        NumberPlate oldEntity = repository.findByCarNumber(tmpNumber).orElse(null);
        if(oldEntity == null){
            log.warn("IN getNextNumberAfterFixed - not found entity from requestDto");
        }
        assert oldEntity != null;
        NumberPlate newEntity = repository.findAllByPosition(oldEntity.getPosition() + 1).orElse(null);
        if(newEntity == null){
            log.warn("IN getNextNumberAfterFixed - not found next entity");
        }
        assert newEntity != null;
        log.info("IN getNextNumberAfterFixed - send number with position {} after {}",
                newEntity.getPosition(), oldEntity.getPosition());
        return mapper.toDto(newEntity);
    }
}
