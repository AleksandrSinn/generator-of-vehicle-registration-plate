package com.project.demo.service;

import com.project.demo.dto.NumberPlateRequestDto;
import com.project.demo.dto.NumberPlateResponseDto;


public interface NumberPlateService {
     NumberPlateResponseDto getRandomNumber();
     NumberPlateResponseDto getNextNumber();
     NumberPlateResponseDto getNextNumberAfterFixed(NumberPlateRequestDto requestDto);
}
