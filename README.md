# Generator of NumberPlates

 >This program allows us to take a numberplate. You can use random or next method.
 Next method start with "A000AA 116 rus" by default. If you send the body in post request,
 you will take a next number standing after your request. The last numberplate is "X999XX 116 rus".

# Data base
In this project used PostgresQl. The schema of tables: ![](src/main/resources/img/photo_db-structure_number_plates.png)
To start changelog_01 you may write: mvn liquibase:update 

![](src/main/resources/img/photo_db_start.png)

![](src/main/resources/img/photo_db_final.png)
# How it works
* First you wait, that dataBase is fulling of numberPlates it's about 12+- minutes.
* In Logs, you will see: ![](src/main/resources/img/photo_logging_inizialise.png)
* When your dataBase is full, you can comment two rows in class SqlInitializationComponent, method run: 
   1) repository.deleteAll()
   2) this.initializeDbOfAllNumberPlates()
![](src/main/resources/img/photo_applicationRunner_run.png)
 
- http://localhost:2020/number/next - to take next numberplate(get/post)
- http://localhost:2020/number/random - to take random numberplate
- You may test in swagger: http://localhost:2020/swagger-ui/index.html